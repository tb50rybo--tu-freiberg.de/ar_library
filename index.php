<!-- 
    Dependencies: 
-->

<script src="js/ar.js"></script>
<script src="js/aframe-master.min.js"></script>
<script src="js/aframe-ar.js"></script>

<!-- 
    navclick-function: In order to actually "click" (or tap) on the right book you have
    to position your smartphone/tablet/camera so the black ring in the center is
    hovering over the book you want to check out.
-->

<script>
    AFRAME.registerComponent("navclick", {
        schema: {
            url: {
                default: ""
            }
        },
        init: function () {
            var data = this.data;
            var el = this.el;
            el.addEventListener("click", () => {
                window.open(data.url, "_blank");
            });
        }
    });
</script>

<body style="margin : 0px; overflow: hidden;">
    <a-scene embedded arjs="detectionMode: mono_and_matrix; matrixCodeType: 4x4_BCH_13_9_3;">
        <a-assets>
            <?php

            $jsonBooks = file_get_contents('books.json');
            $jsonConfig = file_get_contents('config.json');
            $jsonBooks = json_decode($jsonBooks, true);
            $jsonConfig = json_decode($jsonConfig, true);

            // Create Assets: 
            // iterate over all categories
            echo "\n";
            for ($i = 0; $i < count($jsonBooks["categories"]); $i++) {
                $books = $jsonBooks["categories"][$i]["books"];
                foreach ($books as $book) {
                    if ($book["id"] != "skip")
                        echo "            <img id='" . $book["id"] . "' src='" . $book["image"] . "'>\n";
                }
            }
            echo "\n        </a-assets>\n\n";

            // iterate over all categories - each category gets its own marker!
            for ($i = 0; $i < count($jsonBooks["categories"]); $i++) {
                echo "        <a-marker type='barcode' value='" . $i . "'>\n";

                $books = $jsonBooks["categories"][$i]["books"];

                // place every single book according to the pre-defined position from the 'config.json'
                $j = 0;
                foreach ($books as $book) {
                    if ($book["id"] != "skip")
                        print_r("            <a-box src='#" . $book["id"] . "' scale='" . $jsonConfig["scale"] . "' rotation='" . $jsonConfig["rotation"] . "' position='" . $jsonConfig["position"][$j + 1] . "' navclick='url: " . $book["url"] . "'></a-box>\n");
                    $j++;
                }
                echo "        </a-marker>\n\n";
            }
            ?>
        </a-assets>

        <a-camera rotation-reader>
            <a-entity cursor="fuse: false;rayOrigin:mouse;" raycaster="objects:a-box" position="0 0 -1"
                geometry="primitive: ring; radiusInner: 0.02; radiusOuter: 0.03" material="color: black; shader: flat">
            </a-entity>
        </a-camera>
    </a-scene>
</body>