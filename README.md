# ar_library

AR Library - ein Augmented Reality-Tool zur mobilen Sichtbarmachung von eBooks, Sammelwerken und allerlei referenzierten Ressourcen

![info](images/info.gif)

## Was ist AR Library?

- Ein Projekt der UB Freiberg (TUBAF)
    - ursprünglich: AR-Showcase für die Nacht der Wissenschaft und Wirtschaft 2024 https://www.ndww-freiberg.de/
    - Code ist mit Hinblick auf potentielle Nachnutzbarkeit modularisiert worden
    
- Anpassbarkeit
    - Einbau eigener Sammlungen mit Buchcover-Bildern in `books.json` bzw. im `images`-Ordner möglich
    - Einstell-Parameter siehe `config.json`

- Dieses Projekt basiert auf folgenden frei verfügbaren Codebasen:
    - AR.js (MIT-License) https://ar-js-org.github.io/AR.js-Docs/ bzw. https://github.com/ar-js-org
    - AFRAME (als Abhängigkeit von AR.js, MIT-License) https://github.com/AR-js-org/aframe

- Die AR-Marker (im Ordner `barcodes`) sind eine Spiegelung der `AR_MATRIX_CODE_4x4_BCH_13_9_3`-Codes von https://github.com/nicolocarpignoli/artoolkit-barcode-markers-collection

- Dieses Projekt ist in Teilen inspiriert von folgendem Tutorial des Makerspace Gießen:
    - https://makerspace-giessen.de/2021/05/26/tutorial-eine-eigene-augmented-reality-ar-web-anwendung-erstellen/

## Mach' deine eigene AR Library!

Um eine grobe Nutzungsidee zeigen zu können, sind zwei Beispielkategorien mit Dummy-Bildern hinterlegt. Neben des Kategorienamens sind auch die einzelnen verlinkten Inhalte (welche als `books` zusammengefasst sind) anpassbar. Hierbei hat jedes einzelne "Buch" eine eindeutige `id`, ein Coverbild `image` sowie eine Verlinkung im Katalog `url`. Zur Illustration nachfolgend ein Auszug aus der `books.json`:

```json

{
    "categories":
    [
        {
            "category": "Beispiel 1 - Titel, welche mit ihrem Zahlwort korrespondieren",
            "books": [
                {
                    "id": "001",
                    "image": "images/001.jpg",
                    "url": "https://katalog.ub.tu-freiberg.de/Record/0-680325786"
                },
                // ...
            ]
        }
    ],
    // ...
}

```

### Crashkurs - Anpassung der AR Library
- Austausch der Beispielbilder im `images`-Ordner
- Editieren der `books.json` nach eigenem Belieben
    - Nutzung der AR-Marker (bzw. Barcodes) in Verbindung mit den einzelnen Kategorien:
        - alle Barcodes des Typs `4x4_BCH_13_9_3` korrespondieren mit den Ordnungszahlen `0` bis `511`
            - diese sind im Ordner `barcodes` des Repositorys zu finden
            - wenn Barcodes gedruckt werden: Idealerweise auf weißen Papier bzw. mit einem weißen Rand versehen!
        - in der gegebenen Konfiguration können maximal `512` Kategorien à `8` Bücher/Links referenziert werden
            - große Kategorien sollten in mehrere kleinere Kategorien unterteilt werden
            - eine Kategorie soll (vor allem der Übersichtlichkeit wegen) nicht mehr als `8` einzelne Bücher/Referenzen beinhalten

- Die Anordnung der einzelnen Bücher innerhalb einer Kategorie sieht wie folgt aus:
```
1 2 3
4   5
6 7 8
```

- beinhaltet eine Kategorie weniger als 8 Bücher, kann mit `"id": "skip"` die entsprechende Stelle übersprungen werden.
- Upload der Codebasis auf einen PHP-fähigen Server (LAMP-Stack, XAMPP, ...)
- Viel Vergnügen :)

## Nutzung von AR-Markern innerhalb eines QR-Codes

Ein QR-Code zeigt für gewöhnlich auf eine Website. Zur einfacheren Handhabung für Smartphone- und Tablet-Nutzer kann die Zielwebsite als QR-Code codiert werden. Innerhalb dieses QR-Codes können die AR-Marker als "Logo" eingebettetet werden. So ist es den Nutzern möglich direkt mit der Kamerafunktion des jeweiligen Geräts die Website aufzurufen und gleichzeitig können die einzelnen Werke nun entsprechendend gesehen werden.

- Sollte die Nutzung eines eingebetteten AR-Marker gewünscht sein, so sind folgende Hinweise zu beachten:
    - Mittels eines kostenfreien Tool wie z.B. https://www.qrcode-monkey.com/ kann ein AR-Marker in einen QR-Code eingebettet werden
    - Damit der AR-Marker nicht den QR-Code überdeckt, sollte das jeweilige AR-Marker-Bild mit einem weißen Rand versehen werden und erst dann hochgeladen werden

## Troubleshooting
- Smartphones/Tablets:
    - Nutzung des Standardbrowsers (iPhone/iPad = Safari, Android = Chrome) wird zwingend empfohlen
    - bei iPads/iPhones: Warnung wegklicken

## P.S.:

Über einen Backlink würden wir uns sehr freuen!